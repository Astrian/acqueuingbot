// Frameworks
const debug = require('debug')('acqb:interface/dismiss.js')

// Database plugin
const database = require('../plugins/database')

module.exports = async (initiator) => {
  // Find which queue should be destroyed
  initiator = await database('r', `SELECT * FROM user WHERE tgid = '${initiator}'`)
  initiator = initiator[0]
  let queue = await database('r', `SELECT * FROM queue WHERE initiator = '${initiator.id}' AND status = 0`)
  if (!queue.length) return { requestormsg: '你现在没有开启任何联机队列。' }
  queue = queue[0]

  // Find all user wating in the queue
  let members = await database('r', `SELECT user FROM member WHERE queue = '${queue.id}' AND (status = 0 OR status = 1 OR status = 2)`)
  for (let i in members) {
    members[i] = await database('r', `SELECT * FROM user WHERE id = '${members[i].user}'`)
    members[i] = members[i][0]
  }

  // Edit status
  await database('w', `UPDATE member SET status = -1 WHERE queue = '${queue.id}' AND (status = 0 OR status = 1 OR status = 2)`)
  await database('w', `UPDATE queue SET status = -1 WHERE id = '${queue.id}'`)

  return {
    requestormsg: '你的队列已解散。',
    members: {
      list: members,
      message: `${initiator.firstname}${initiator.tgusername ? (' (@' + initiator.tgusername + ')') : ''} 已将队列解散，你已从解散的队列中退出。`
    }
  }
}