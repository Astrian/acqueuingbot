// Frameworks
const debug = require('debug')('acqb:interface/end.js')

// Database plugin
const database = require('../plugins/database')

module.exports = async (user) => {
  // Get the queue user joined
  let sth = await database('r', `SELECT * FROM user WHERE tgid = '${user}'`)
  debug(sth)
  user = sth[0]
  let queuemember = await database('r', `SELECT * FROM member WHERE user = '${user.id}' AND (status = 1 OR status = 0 OR status = 2)`)
  if (!queuemember.length) return { requester: '你当前不在任何队列中。和朋友约一下吧！' }
  queuemember = queuemember[0]

  // Check if user now activating
  let isActive = queuemember.status

  // Edit status
  await database('w', `UPDATE member SET status = -1 WHERE id = '${queuemember.id}'`)

  if (isActive) {
    // Get all lists from current waiting list
    let waitinglist = await database('r', `SELECT * FROM member WHERE queue = '${queuemember.queue}' AND status = 0 ORDER BY jointime`)

    // Get queue info
    let queue = await database('r', `SELECT * FROM queue WHERE id = '${queuemember.queue}'`)
    queue = queue[0]

    // Get initiator's information for send notification
    let initiator = await database('r', `SELECT * FROM user WHERE id = '${queue.initiator}'`)
    initiator = initiator[0]

    // If waiting list is empty, destroy the queue
    if (!waitinglist.length) {
      // Check if other people is activing or holding... 
      let activatinglist = await database('r', `SELECT * FROM member WHERE queue = '${queuemember.queue}' AND (status = 1 OR status = 2)`)
      debug('activatinglist')
      debug(activatinglist)
      debug('===========')

      if (!activatinglist.length) { 
        // Destory queue
        await database('w', `UPDATE queue SET status = -1 WHERE id = '${queue.id}'`)
        return {
          requester: `已结束联机。由于队列已空，因此该队列已被机器人关闭。`,
          initiator: {
            tgid: initiator.tgid,
            msg: `${user.firstname}${user.tgusername ? (' (@' + user.tgusername + ')') : ''} 已结束与你联机。联机队列已空，因此队列已被机器人关闭。重新发送 <pre>/new <联机密码> <最大联机人数></pre> 来开启新的联机队列。`,
          }
        }
      } else {
        // Not destory queue
        return {
          requester: `已结束联机。若需重新在此队列排队，请发送 <pre>/start ${queue.id}</pre> 指令。`,
          initiator: {
            tgid: initiator.tgid,
            msg: `${user.firstname}${user.tgusername ? (' (@' + user.tgusername + ')') : ''} 已结束与你联机。当前队列中无人等待，但有人正与你联机或在 hold 状态，因此队列将继续存在，且机器人不会将联机密码发送给新的用户。`,
          }
        }
      }
    } else {
      let newactuser
      
      // First one in waiting list can now active. Get this user info and change status flag
      newactuser = await database('r', `SELECT * FROM user WHERE id = '${waitinglist[0].user}'`)
      newactuser = newactuser[0]
      await database('w', `UPDATE member SET status = 1 WHERE id = '${waitinglist[0].id}'`)

      // Get initiator's information for send notification
      let initiator = await database('r', `SELECT * FROM user WHERE id = '${queue.initiator}'`)
      initiator = initiator[0]
      debug(`queue.initiator = ${queue.initiator}`)

      return {
        requester: `已结束联机。若需重新在此队列排队，请发送 <pre>/start ${queue.id}</pre> 指令。`,
        initiator: {
          tgid: initiator.tgid,
          msg: `${user.firstname}${user.tgusername ? (' (@' + user.tgusername + ')') : ''} 已结束与你联机。下一位联机用户是 ${newactuser.firstname}${newactuser.tgusername ? (' (@' + newactuser.tgusername + ')') : ''}，机器人已自动向这位用户发送联机密码。`,
        },
        next: {
          tgid: newactuser.tgid,
          msg: `<b>已轮到你联机！</b>\n联机密码：<pre>${queue.password}</pre>`
        }
      }
    }
  } else {
    return {
      requester: `已从队列退出并停止继续等待。若需重新在此队列排队，请发送 <pre>/start ${queue.id}</pre> 指令。`
    }
  }
}