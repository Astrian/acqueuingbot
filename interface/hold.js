// Frameworks
const debug = require('debug')('acqb:interface/hold.js')

// Database plugin
const database = require('../plugins/database')

module.exports = async (requestor) => {
  // Get user's info
  requestor = await database('r', `SELECT * FROM user WHERE tgid = '${requestor}'`)
  requestor = requestor[0]

  // Find is holdable or unholdable
  let target = await database('r', `SELECT * FROM member WHERE user = '${requestor.id}' AND (status = 0 OR status = 2)`)
  if (!target.length) return '暂离指令用于在排队时，参与者临时有事或无法及时参与联机的情况，它可以提高轮流联机效率。\n当你在队列排队中时，向机器人发送 /hold 指令，机器人将会保留你在队列中的位置。若轮到你，但你还没有准备好联机，机器人会将联机机会顺延至排在你之后的用户。\n重新发送 /hold 将会取消暂离，此时机器人会将你排列在原有位置或队首。'
  target = target[0]

  // Hold or unhold
  if (target.status === 0){
    await database('w', `UPDATE member SET status = 2 WHERE id = '${target.id}'`)
    return { requestormsg: '机器人已保留你在队列中的位置。若轮到你，但你还没有准备好联机（取消暂离状态），机器人会将联机机会顺延至排在你之后的用户。\n重新发送 /hold 取消暂离状态。' }
  } else {
    // Is anyone wating in the list?
    let waitinglist = await database('r', `SELECT * FROM member WHERE queue = '${target.queue}' AND status = 0`)
    if (waitinglist.length) {
      await database('w', `UPDATE member SET status = 0 WHERE id = '${target.id}'`)
      return {requestormsg: '暂离状态已被取消，正常回到队列等待。' }
    } else {
      // get queue info
      let queue = await database('r', `SELECT * FROM queue WHERE id = '${target.queue}'`)
      queue = queue[0]

      // get active member
      let sth = await database('r', `SELECT * FROM member WHERE queue = '${target.queue}' AND status = 1`)

      if (parseInt(queue.max) > parseInt(sth.length)) {
        await database('w', `UPDATE member SET status = 1 WHERE id = '${target.id}'`)
        
        // get password and initiator info
        let initiator = await database('r', `SELECT * FROM user WHERE id = '${queue.initiator}'`)
        initiator = initiator[0]

        return {
          requestormsg: `<b>轮到你了！</b>\n使用联机密码 <pre>${queue.password}</pre> 进行联机。`,
          initiator: {
            tgid: initiator.tgid,
            msg: `${requestor.firstname}${requestor.tgusername ? (' (@' + requestor.tgusername + ')') : ''} 已结束暂离状态并与你开始联机。机器人已将联机密码发送给对方。`
          }
        }
      } else {
        await database('w', `UPDATE member SET status = 0 WHERE id = '${target.id}'`)
        return {requestormsg: '已从暂离状态恢复，将正常回到队列等待。\n' }
      }
    }
  }
}