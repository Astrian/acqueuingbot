// Frameworks
const debug = require('debug')('acqb:interface/share.js')

// Database plugin
const database = require('../plugins/database')

module.exports = async (id) => {
  // Get uuid for this user
  let db = await database('r', `SELECT id FROM user WHERE tgid = '${id}'`)
  if (!db.length) throw 'no this user'

  // Get queue for this user
  db = await database('r', `SELECT id FROM queue WHERE initiator = '${db[0].id}' AND status = 0`)
  if (!db.length) return { available: false }

  // Parse queue info
  let queue = db[0]
  let res = {
    available: true,
    queueid: queue.id
  }
  return res
}