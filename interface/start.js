// Frameworks
const debug = require('debug')('acqb:interface/start.js')
const uuid = require('uuid')

// Database plugin
const database = require('../plugins/database')

module.exports = async (id, fn, ln, un) => {
  // Find the user from database, or register a new item for this user
  let sth = await database('r', `SELECT * FROM user WHERE tgid = '${id}'`)
  if (!sth.length) {
    await database('w', `INSERT INTO user (id, tgid, firstname, lastname, tgusername, time) VALUES ('${uuid.v4()}', '${id}', '${fn}', '${ln || ''}', '${un || ''}', '${new Date().getTime()}')`)
  } else {
    await database('w', `UPDATE user SET firstname = '${fn}', lastname = '${ln || ''}', tgusername = '${un || ''}' WHERE tgid = '${id}'`)
  }
  // Output help info
  let output = ''
  output += '<b>欢迎使用动森叫号器</b>\n'
  output += '动森叫号器可以帮助您解决您的无人岛上出现大头菜好价时，水友上岛的排队难题。\n'
  output += '使用 `/new [联机密码] [最大同时联机人数]` 指令发起新的队列。\n'
  return output
}