// Frameworks
const debug = require('debug')('acqb:interface/update.js')

// Database plugin
const database = require('../plugins/database')

module.exports = async (initiator, password, max) => {
  debug(`user ${initiator} request to update queue...`)
  // Check if user created a new queue
  initiator = await database('r', `SELECT * FROM user WHERE tgid = '${initiator}'`) 
  initiator = initiator[0]
  let queue = await database('r', `SELECT * FROM queue WHERE initiator = '${initiator.id}'`)
  if (!queue.length) return { requestor: '你没有发起联机队列。尝试使用 `/new [联机密码] [最大同时联机人数]` 来发起一个新队列吧。' }
  queue = queue[0]
  debug('this user created a new queue.')

  // Verify input
  if (!password || !max) {
    debug('no password or max num found.')
    debug(`password = ${password}`)
    debug(`max = ${max}`)
    let res = ''
    res += '请发送 `/update [联机密码] [最多同时联机人数]` 指令来修改联机信息。'
    return { requestor: res }
  }

  // Verify format of password
  if (password.length !== 5) {
    let res = ''
    res += '<b>密码格式不正确</b>\n'
    res += '正确的密码应该是五位数字、英文字母（除 I、O、Z 外）组合。'
    return { requestor: res }
  }

  // Verify max member
  if (isNaN(max) || max < 1 || max > 7) {
    let res = ''
    res += '<b>最多同时访问人数不正确</b>\n'
    res += '最低 1 人联机、最多 7 人联机。建议将此值设为 2。'
    return { requestor: res }
  }

  debug('completed input verify. updating database...')
  await database('w', `UPDATE queue SET password = '${password}', max = '${max}' WHERE id = '${queue.id}'`)

  debug('data updated. send notifications to queue member...')
  let members = await database('r', `SELECT * FROM member WHERE queue = '${queue.id}' AND status = 1`)
  for (let i in members) {
    members[i] = await database('r', `SELECT tgid FROM user WHERE id = '${members[i].user}'`)
    members[i] = members[i][0].tgid
  }

  return {
    requestor: `联机信息已更新，新密码是 <pre>${password}</pre>，最大联机人数是 ${max}（当前正在联机的用户不受影响）。`,
    members: {
      list: members,
      msg: `${initiator.firstname}${initiator.tgusername ? (' (@' + initiator.tgusername + ')') : ''} 已更新联机密码，新密码是 <pre>${password}</pre>。`
    }
  }
}